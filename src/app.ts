import express from "express";
import { config } from './configs/index'

const app = express();

app.get("/", (req, res) => {
  res.send("Hola Pragma!");
});

app.listen(config.port, () => {
  console.log(`Servidor en puerto: ${config.port}`);
}).on('error', (e) => {
    console.log("Ocurrió un error: ", e.message);
});
